
# AAVS System

This is the base repository for the AAVS software systems, organised as follows:

- bitfiles: TPM Firmware bitfiles
- config: Station configuration files
- python/pyaavs: Station monitoring and control
- python/pydaq: Data acquisition
- python/aavs_calibration: Calibration database
- python/utilities: Useful scripts
- src: Data acqusition backend
- tests: Firmware and software tests
- web: Monitoring page